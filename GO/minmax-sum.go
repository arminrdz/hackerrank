/*
Given five positive integers, find the minimum and maximum values that can be calculated by summing exactly four of the five integers.
Then print the respective minimum and maximum values as a single line of two space-separated long integers.
For example, arr[1,3,5,7,9] . Our minimum sum is 1+3+5+7=16  and our maximum sum is 3+5+7+9=24 . We would print 16 24

Sample input
1 2 3 4 5

Sample output
10 14

*/

package main

import (
	"fmt"
	"log"
	"sort"
)

func main() {


	data, err := getArray()

	if err != nil {
		log.Fatal(err)
	}


	 sort.Ints(data)
//	 lowest := data[0]
	 indexLowest := 0
	 indexGreatest := len(data) - 1
//	 greatest := data[indexGreatest]

//	 fmt.Println(data)
//	  fmt.Println(lowest)
//	 fmt.Println(greatest)

//	 fmt.Println(indexLowest)
//	fmt.Println(indexGreatest)


	 minSum := sumArray(data, indexGreatest)

	 if minSum != 0 {

		 maxSum := sumArray(data, indexLowest)

		 fmt.Println(minSum, maxSum)

	 }




}


func sumArray(a []int, index int) (result int) {
	var sum int
	arr1 := RemoveIndex(a, index)
	  for _, v := range arr1{
	  	sum += v
		}
	return sum



}




func RemoveIndex(s []int, index int) []int {
	return append(s[:index], s[index+1:]...)
}

func getArray() ([]int, error)    {
	data := make([]int, 5)
	for i:= range data {
		_, err := fmt.Scanf("%d", &data[i])
		if err != nil {
			return nil, err
		}
	}
	return data, nil
}
