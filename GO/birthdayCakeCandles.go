/*
You are in charge of the cake for your niece's birthday
and have decided the cake will have one candle for each year
of her total age.
When she blows out the candles, she’ll only be able to blow out
the tallest ones.
Your task is to find out how many candles
she can successfully blow out.
For example, if your niece is turning 4 years old, and
the cake will have 4 candles of height 4,4,1,3
she will be able to blow out 2 candles successfully,
since the tallest candles are of height 4 and there are 2 such candles.
 */

package main

import (
	"fmt"
	"log"
	"sort"
)

func main () {


	data, err := getCandles()

	var candles int

	if err != nil {
		log.Fatal(err)
	}

	 sort.Sort(sort.Reverse(sort.IntSlice(data)))
	// fmt.Println(data)


	for i:= 0; i< len(data); i++ {

		if data[0] == data[i] {

			candles += 1
		}

	}


	fmt.Println(candles)


}

func getCandles() ([]int, error)  {

	var arraySize int

	_, err := fmt.Scanf("%d",&arraySize)

		if err != nil {
			return nil, err
		}

		data := make([]int, arraySize)


		for i := range data {

			_, err := fmt.Scanf("%d", &data[i])

			if err != nil {
				return nil, err
			}
		}

		return data, nil


}