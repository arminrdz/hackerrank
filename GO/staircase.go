/*
Consider a staircase of size n=4:
   #
  ##
 ###
####
Observe that its base and height are both equal to n , and the image is drawn using # symbols and spaces.
The last line is not preceded by any spaces.
Write a program that prints a staircase of size n .
*/

package main

import (
	"fmt"
	"strings"
)

func main() {
	var number int
	number = getInteger()
	for i := 1; i <= number; i++ {
		fmt.Printf("%s%s\n", strings.Repeat(" ", number-i), strings.Repeat("#", i))
	}
}


func getInteger ()int {
	var n int
	_, err := fmt.Scan(&n)
	if err != nil {
		panic(err)
	}
	return n
}