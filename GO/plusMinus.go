/*
Given an array of integers, calculate the fractions of its elements that are positive, negative, and are zeros.
Print the decimal value of each fraction on a new line.
You must print the following  lines:
A decimal representing of the fraction of positive numbers in the array compared to its size.
A decimal representing of the fraction of negative numbers in the array compared to its size.
A decimal representing of the fraction of zeros in the array compared to its size.
Sample Input
6
-4 3 -9 0 4 1
Sample Output
0.500000
0.333333
0.166667
*/

package main

import "fmt"

func main(){

	var arraySize int
	arraySize = read()



	var array []int

		for i:=0; i < arraySize; i++ {
			arrayItem := read()
			array = append(array, arrayItem)
		}

		var positiveNumbers, negativeNumbers, zeros int

			for x:=0; x<arraySize; x++ {

				if array[x] < 0 {

					negativeNumbers += 1

				}

				if array[x] > 0 {

					positiveNumbers += 1

				}

				if array[x] == 0 {

					zeros += 1
				}

			}

	fmt.Println(float64(positiveNumbers)/float64(arraySize))
	fmt.Println(float64(negativeNumbers)/float64(arraySize))
	fmt.Println(float64(zeros)/float64(arraySize))


}

func read ()int {
	var n int
	_, err := fmt.Scan(&n)

	if err != nil {
		panic(err)
	}
	return n
}
