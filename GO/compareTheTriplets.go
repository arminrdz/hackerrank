/*
Alice and Bob each created one problem for HackerRank. A reviewer rates the two challenges,
awarding points on a scale from to for three categories: problem clarity, originality, and difficulty.

We define the rating for Alice's challenge to be the triplet challenge to be the triplet .
A=(a[0],a[1],a[2]) and the rating for Bob's challenge to be the triplet  B=(b[0],b[1],b[2])

Your task is to  find their comparison points by comparing  a[0] with b[0] ....

If a[i]> b[i] , then Alice is awarded point.
If a[i]< b[i], then Bob is awarded point.
If a[i]= b[i], then neither person receives a point.
Comparison points is the total points a person earned.
Given a and b , determine their respective comparison points.

Sample Input
5 6 7
3 6 10

Sample Output
1 1 

 */

package main

import (
	"fmt"
	"log"
)

func main()  {

	aliceData, err := getValues()
	bobData, err := getValues()

	if err != nil {
		log.Fatal(err)
	}

	alice, bob := compareValues(aliceData, bobData)

	fmt.Println(alice, bob)


}


func compareValues(a[]int, b[]int) (int, int) {

	var alice, bob int


	if a[0] > b[0] {

		alice += 1
	}

	if a[1] > b[1] {

		alice += 1
	}

	if a[2] > b[2] {

		alice += 1
	}

	if a[0] < b[0] {

		bob +=1
	}

	if a[1] < b[1] {

		bob +=1
	}

	if a[2] < b[2] {

		bob +=1
	}


	return alice, bob

}



func getValues() ([]int, error)  {

	data := make([]int, 3)

	for i := range data {

		_, err := fmt.Scanf("%d", &data[i])

		if err != nil {
			return nil, err
		}
	}

	return data, nil

}